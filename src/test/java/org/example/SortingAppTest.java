package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppTest {
    private final String[] input;
    private final int[] expected;

    public SortingAppTest(String[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, new int[]{}},
                {new String[]{"5"}, new int[]{5}},
                {new String[]{"8", "4", "2", "6", "7", "1", "9", "3", "0", "5"}, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}},
                {new String[]{"2", "3", "8", "4", "0", "5", "1", "10", "6", "7", "9"}, new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
        });
    }

    @Test
    public void testSorting() {
        SortingApp.main(input);
        assertArrayEquals(expected, SortingApp.getArray());
    }
}