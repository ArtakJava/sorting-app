package org.example;

import java.util.Arrays;

public class SortingApp {
    private static int[] numbers;

    public static void main( String[] args ) {
        numbers = new int[args.length];
        if (args.length == 0) {
            System.out.println("No arguments provided.");
            return;
        }
        for (int i = 0; i < args.length; i++) {
            numbers[i] = Integer.parseInt(args[i]);
        }

        Arrays.sort(numbers);
    }

    public static int[] getArray() {
        return numbers;
    }
}